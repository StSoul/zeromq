####Dependences
apt-get install libzmq3-dev
apt-get install build-essential


node index.js --pub=3000 --sub=3001 --db-host="localhost" --db-port="3306" --db-name="testDb" --db-pwd="mysql"

pm2 start index.js -log-date-format 'DD-MM HH:mm:ss' --name "cron" -- --pup=3005 --sub=3005 --db-host="localhost" --db-port="3306" --db-name="testDb" --db-pwd="mysql"