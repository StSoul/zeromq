const sock = require('zmq').socket('sub');

sock.connect(`tcp://127.0.0.1:3000`);
sock.subscribe('api_out');

console.log(`Subscriber connected to port 3000`);

sock.on('message', async (topic, message) => {
   console.log('received a message related to:', topic.toString(), 'containing message:', message.toString());
});

module.exports = sock;