const sock = require('zmq').socket('pub');

sock.bindSync(`tcp://127.0.0.1:3001`);
console.log(`test pup bound to port 3001`);

setInterval(function () {
   const msg = {
      type: 'login',
      email: 'foo@bar.baz',
      pwd: 'x1',
      msg_id: 1,
   };
   sock.send(['api_in', JSON.stringify(msg)]);
}, 2000);