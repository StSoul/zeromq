'use strict';

let dbm;
let type;
let seed;

const async = require('async');
const md5 = require('md5');

exports.setup = function (options, seedLink) {
   dbm = options.dbmigrate;
   type = dbm.dataType;
   seed = seedLink;
};

const sql = `INSERT INTO user (email, passw) VALUES ("foo@bar.baz", "${md5('x')}")`;

exports.up = function (db, callback) {
   async.series([
      db.createTable.bind(db, 'user', {
         user_id: {type: 'int', primaryKey: true, autoIncrement: true, notNull: true, unsigned: true},
         email: {type: 'string', notNull: true, length: 25},
         passw: {type: 'string', notNull: true, length: 60},
      }),
      db.runSql.bind(db, sql, []),

   ], callback);
};

exports.down = function (db, callback) {
   async.series([
      db.dropTable.bind(db, 'user', {ifExists: true}),
   ], callback);
};

exports._meta = {
   "version": 1
};
