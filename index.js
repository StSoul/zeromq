const conf = require(__dirname + '/config.json');
const mysql = require('promise-mysql');
const argv = require('minimist')(process.argv.slice(2));


global.config = {
   zero: {
      pabPort: argv.pub || conf.zero.pub,
      subPort: argv.sub || conf.zero.sub,
      host: argv['broker-host'] || conf.zero.host,
   },
   dbConf: {
      host: argv['db-host'] || conf.mysql.host,
      user: argv['db-user'] || conf.mysql.user,
      port: argv['db-port'] || conf.mysql.port,
      database: argv['db-name'] || conf.mysql.database,
      password: argv['db-pwd'] || conf.mysql.password,
   }
};

global.mysql = mysql.createPool(global.config.dbConf);

require(__dirname + '/socket/index');



