const sock = require('zmq').socket('sub');
const port = global.config.zero.subPort;

sock.connect(`tcp://127.0.0.1:${port}`);
sock.subscribe('api_in');

console.log(`Subscriber connected to port ${port}`);

module.exports = sock;