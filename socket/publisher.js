const sock = require('zmq').socket('pub');
const port = global.config.zero.pabPort;

sock.bindSync(`tcp://127.0.0.1:${port}`);
console.log(`Publisher bound to port ${port}`);

module.exports = sock;