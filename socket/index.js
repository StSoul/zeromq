const pub = require('./publisher');
const sub = require('./subscriber');
const md5 = require('md5');

sub.on('message', async (topic, message) => {
   console.log('received a message related to:', topic.toString(), 'containing message:', message.toString());

   let data = {};

   try {
      data = JSON.parse(message.toString());
   } catch (err) {
      console.error(err);
      return;
   }

   if (topic.toString() === 'api_in' && data.type === 'login') {

      const res = {};

      if (!('msg_id' in data) || !('email' in data) || !('pwd' in data)) {
         res.msg_id = data.msg_id || null;
         res.status = 'error';
         res.error = 'WRONG_FORMAT';
         pub.send(['api_out', JSON.stringify(res)]);
         return;
      }

      const pass = md5(data.pwd);
      const user = await global.mysql.query('SELECT * FROM user WHERE email = ? AND passw = ?', [data.email, pass]);

      console.log(user);

      res.msg_id = data.msg_id;

      if (!user.length) {
         res.status = 'error';
         res.error = 'WRONG_PWD';
      } else {
         res.status = 'ok';
         res.user_id = user[0].id;
      }

      pub.send(['api_out', JSON.stringify(res)]);
   }
});